final color ALIEN1=color(0,255,0);
final color ALIEN2=color(50,100,0);

class Alien
{
  float x = width - 25;
  float y;
  float speedx;
  float speedy;
  
  Alien(float y, float speedx, float speedy)
  {
    this.y = y;
    this.speedx = speedx;
    this.speedy = speedy;
  }
  
  void render()
  {
    fill(ALIEN1);
    ellipse(x,y,30,30);
    fill(ALIEN2);
    ellipse(x,y,50,15);
  }
  
  void move()
  {
    x = x - speedx;
    y = y + random(-10, 10);
    if (x < 25)
      {
        x = width + random(500);
        y = height/2;
      }
    else if (y > height-25)
      {
        y = y - random(1,10);
      }
    else if (y<0)
      {
        y = y + random(1,10);
      }
  }
}