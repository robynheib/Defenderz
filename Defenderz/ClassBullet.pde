class Bullet
{
  float x = 70;
  float y = 0;
  float speedx;
  
 Bullet(float x, float y, float speedx)
 {
    this.y = y;
    this. speedx = speedx;
 }
 
 void initiate(float defendery)
 {
   y = defendery;
   x = 70;
 }
 
 void render()
 {
   fill(255,255,0);
   rect(x,y,10,10);
   x = x + speedx;
   if (x == width - 10)
   {
     bulletFired = false;
   }
 }
 
 void crash(Alien alien)
 {
   if (abs(this.x - alien.x)<20 && abs(this.y - alien.y)<20)
   {
     alien.x = width + random(50);
     alien.y = height/2;
     bulletFired = false;
     score = score + 1;
   }
 }
}