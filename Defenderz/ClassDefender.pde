class Defender
{
  float x = 50;
  float y;
  float speedy;
  
 Defender(float y, float speedy)
 {
    this.y = y;
    this. speedy = speedy;
 }
 
 void render()
 {
   fill(255,0,0);
   rect(x,y,50,20);
   triangle(x+50,y,x+50,y+20,x+60,y+10);
   fill(0,0,100);
   rect(x,y-10,20,10);
 }
 
 boolean crash()
 {
   for (int i = 0; i <30; i++)
   {
     if (get((int)(x+50),(int)(y-10+i)) == ALIEN1 || get((int)(x+50),(int)(y-10+i)) == ALIEN2)
     {
       return true;
     }
   }
   return false;
 }
}