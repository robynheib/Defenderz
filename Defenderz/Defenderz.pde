PImage background;
int x=0;//global variable background location
int gamemode = 0;
int score = 0;
boolean bulletFired = false;
Alien alien1;
Alien alien2;
Defender defender1;
Bullet bullet1;

void setup()
{
  size(1000,500);
  background = loadImage("spaceBackground.jpeg");
  background.resize(width,height);
  alien1 = new Alien(height/2,2,2);
  alien2 = new Alien(height/2,2,2);
  defender1 = new Defender(height/2,2);
  bullet1 = new Bullet(50, height/2,5);
}

void draw()
{
  if (gamemode == 0)
  {
    textSize(50);
    image(background,x,0);
    text("press space to start", (width/2)-230, height/2);
  }
  else if (gamemode == 1)
  {
      image(background,x,0);//draw background twice adjacent
      image(background,x+background.width,0);x-=4;
      if(x== -background.width)
        x=0;//wrap background
      alien1.render();
      alien1.move();
      alien2.render();
      alien2.move();
      defender1.render();
      if (bulletFired == true)
      {
        bullet1.render();
        bullet1.crash(alien1);
        bullet1.crash(alien2);
      }
      fill(255);
      text(score, (width/2), 50);
    if (defender1.crash())
    {
     gamemode = 2;
    }
  }
  else if (gamemode == 2)
  {
    textSize(50);
    text("you crashed press space to restart\nor press Esc to go to main menu", (width/2)-350, height/2);
  }
}


void keyPressed()
{
  if(key == CODED)
  {
    if(keyCode == UP && defender1.y > 0 )
    {
      defender1.y = defender1.y - defender1.speedy;
    }
    if(keyCode == DOWN && defender1.y  < height - 20)
    {
      defender1.y = defender1.y + defender1.speedy;
    }
  }
  if(key == 'w' && defender1.y > 0 )
    {
      defender1.y = defender1.y - defender1.speedy;
    }
  if(key == 's' && defender1.y  < height - 20)
    {
      defender1.y = defender1.y + defender1.speedy;
    }
  if(key == 32)
    {
      if (gamemode == 1)
      {
        bulletFired = true;
        bullet1.initiate(defender1.y);
      }
      else
      {
       gamemode = 1;
       alien1.x = width + random(500);
       alien2.x = width + random(500);
       score = 0;
      }
    }
}  